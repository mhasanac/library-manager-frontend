import { UserProfile } from './../models/user.type';

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { BackendApiPath } from '../../backend/models/backend-api-path.type';
import { BackendService } from '../../backend/services/backend.service';
import { DataResponseContainer } from 'src/app/core/backend/models/data-response-container.type';
import { DataItemService } from 'src/app/core/backend/services/data-item.service';
import { LoggerService } from 'src/app/core/util/logger.service';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class UserService extends DataItemService<UserProfile> {
  constructor(
    private logger: LoggerService,
    private backendService: BackendService
  ) {
    super();
  }

  findData(pageIndex: number, pageSize: number, filterData: any): Observable<HttpResponse<UserProfile[]>> {
    // populate http params
    const httpParams: any = this.getPopulatedHttpParams(
      pageIndex,
      pageSize,
      filterData
    );

    return this.backendService.getJSON<HttpResponse<UserProfile[]>>(
      BackendApiPath.USER_MANAGEMENT_RESOURCE.GET_ALL,
      null,
      httpParams
    );
  }


  getOneByUserName(userName: string): Observable<UserProfile> {
    this.setLoadingDataState(true);
    const getPath = BackendApiPath.USER_MANAGEMENT_RESOURCE.GET_ONE.replace('{userName}', userName);
    return this.backendService.getJSON<UserProfile>(getPath, null, null);
  }

  updateUser(user: UserProfile): Observable<UserProfile> {
    this.setLoadingDataState(true);
    const savePath = BackendApiPath.USER_MANAGEMENT_RESOURCE.CREATE_UPDATE_ONE.replace('{userName}', user.email);
    return this.backendService.putJSON<UserProfile>(savePath, user, null);
  }
}
