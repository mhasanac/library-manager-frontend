import { Injectable } from '@angular/core';
import { Observable, of as ObservableOf } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { BackendService } from '../../backend/services/backend.service';
import { UserProfile, UserLogin } from '../models/user.type';
import { Router } from '@angular/router';
import { LoggerService } from 'src/app/core/util/logger.service';
import { RouteConfig } from '../../../config/route-config';
import { BackendApiPath } from 'src/app/core/backend/models/backend-api-path.type';
import { ConstantUtil } from 'src/app/core/util/constant-util.type';
import { HttpHeaderResponse, HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AuthenticationService {
    constructor(
        private backendService: BackendService,
        private loggerService: LoggerService,
        private router: Router
    ) {}

    goToLoginPage(): void {
        this.router.navigate([RouteConfig.LOGIN], {
            queryParams: { logout: null }
        });
    }

    login(username: string, password: string): Observable<UserProfile> {
        const userLogin: UserLogin = {
            username: username,
            password: password
        };
        return this.backendService
            .postJSON<UserProfile>(
                BackendApiPath.ACCOUNT_RESOURCE.AUTHENTICATE,
                userLogin,
                null
            )
            .pipe(
                map((response: HttpResponse<UserProfile>) => {
                    const headers = response.headers;
                    const authorizationHeader = headers.get(ConstantUtil.AUTHORIZATION_HEADER);
                    // login successful if there's a jwt token in the response
                    if (response.body && authorizationHeader ) {
                        // store username and jwt token in local storage to keep user logged in between page refreshes
                        const user: UserProfile = response.body;
                        user.accessToken = authorizationHeader;
                        localStorage.setItem(
                            ConstantUtil.CURRENT_USER,
                            JSON.stringify(user)
                        );
                    }
                    return response;
                }),
                catchError((err: HttpErrorResponse) => {
                    // const apiError = ApiError.withResponse(err);
                    // this.eventService.publish(EventService.EVENT_API_ERROR, apiError);
                    return ObservableOf(null); // .throw(apiError);
                })
            );
    }

    public logOut(): void {
        // remove user from local storage to log user out
        localStorage.removeItem(ConstantUtil.CURRENT_USER);
        this.goToLoginPage();
    }

    public isAuthenticated(): boolean {
        const currentUser = localStorage.getItem(ConstantUtil.CURRENT_USER);
        return currentUser ? true : false;
    }

    public getAccount(): Observable<UserProfile> {
        return this.backendService.getJSON<UserProfile>(
            BackendApiPath.ACCOUNT_RESOURCE.GET_ACCOUNT,
            null
        );
    }

    public getCurrentUser(): UserProfile {
        const currentUserString = localStorage.getItem(
            ConstantUtil.CURRENT_USER
        );
        if (currentUserString) {
            const jsonObject = JSON.parse(currentUserString);
            const userProfile = new UserProfile(
                jsonObject.id,
                jsonObject.firstName,
                jsonObject.lastName,
                jsonObject.email,
                jsonObject.createdBy,
                jsonObject.createdDate,
                jsonObject.lastModifiedBy,
                jsonObject.lastModifiedDate,
                jsonObject.authorities,
                jsonObject.accessToken
            );
            return userProfile;
        }
        return null;
    }
}
