import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RouteConfig } from '../../../config/route-config';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authenticationService: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const loggedIn: boolean = this.authenticationService.isAuthenticated();
        if (loggedIn) {
            // logged in so return true
            return true;
        }
      // not logged in so redirect to login page with the return url
      this.router.navigate([RouteConfig.LOGIN], { queryParams: { returnUrl: state.url }});
      return false;
    }
}
