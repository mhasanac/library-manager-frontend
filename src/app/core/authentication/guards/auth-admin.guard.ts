import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { RouteConfig } from 'src/app/config/route-config';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthAdminGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const loggedIn: boolean = this.authenticationService.isAuthenticated();
    if (loggedIn) {
      const currentUser = this.authenticationService.getCurrentUser();
      if (currentUser.isAdmin) {
        return true;
      }
      // does not have enough rights
      this.router.navigate([RouteConfig.DASHBOARD], {
        queryParams: { returnUrl: state.url }
      });
    }
    // not logged in so redirect to login page with the return url
    this.router.navigate([RouteConfig.LOGIN], {
      queryParams: { returnUrl: state.url }
    });
    return false;
  }
}
