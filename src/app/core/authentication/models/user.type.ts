export class UserLogin {
  username: string;
  password: string;
}

export class UserRole {
  static ROLE_ADMIN = 'ROLE_ADMIN';
}

export class UserProfile {
  constructor(
      public id: number,
      public firstName: string,
      public lastName: string,
      public email: string,
      public createdBy: string,
      public createdDate: string,
      public lastModifiedBy: string,
      public lastModifiedDate: string,
      public authorities: string[],
      public accessToken: string) {}

  public get isAdmin(): boolean {
      return this.authorities.includes(UserRole.ROLE_ADMIN);
  }

  public get displayName(): string {
      return `${this.firstName} ${this.lastName}`;
  }
}
