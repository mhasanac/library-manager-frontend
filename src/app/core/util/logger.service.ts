import { Injectable } from '@angular/core';
import { Log } from 'ng2-logger/client';

import {MatSnackBar} from '@angular/material';

@Injectable()
export class LoggerService {
  loggerName: string;

  constructor(public snackBar: MatSnackBar) {
  }

  showSuccess(header: string, text: string, toast: boolean) {
    Log.create(this.loggerName).info(header, text);
    this.snackBar.open(text, null, {
      duration: 3000
    });
  }

  showError(header: string, text: string, toast: boolean) {
    Log.create(this.loggerName).error(header, text);
    this.snackBar.open(text, null, {
      duration: 3000
    });
  }

  showWarning(header: string, text: string, toast: boolean) {
    Log.create(this.loggerName).warn(header, text);
    this.snackBar.open(text, null, {
      duration: 3000
    });
  }

  showInfo(header: string, text: string, toast: boolean) {
    Log.create(this.loggerName).info(header, text);
    this.snackBar.open(text, null, {
      duration: 3000
    });
  }

  debug(header: string, text: string) {
    Log.create(this.loggerName).data(header, text);
  }
}
