export class ConstantUtil {
  public static readonly DEFAULT_LOCALE = 'en';
  public static readonly LOCALE_ID = 'localeId';
  public static readonly CURRENT_USER = 'cu';
  public static readonly AUTHORIZATION_HEADER = 'authorization';
  public static readonly X_TOTAL_COUNT_HEADER = 'X-Total-Count';
}
