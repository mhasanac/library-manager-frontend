import { Observable, BehaviorSubject } from 'rxjs';
import { HttpParams, HttpResponse } from '@angular/common/http';

export abstract class DataItemService<T> {
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  abstract findData(
    pageIndex: number,
    pageSize: number,
    filterDataItem: any,
    sortParam?: string
  ): Observable<HttpResponse<T[]>>;

  getPopulatedHttpParams(
    pageIndex: number,
    pageSize: number,
    filterData: any,
    sortParam?: string
  ): any {
    const httpParams: any = {};
    if (typeof pageIndex === 'number' && typeof pageSize === 'number') {
      httpParams['page'] = pageIndex.toString();
      httpParams['size'] = pageSize.toString();
    }

    if (sortParam) {
      httpParams['sort'] = sortParam;
    }

    if (filterData) {
      Object.keys(filterData).forEach(fieldKey => {
        const fieldValue: any = filterData[fieldKey];
        // TODO different types need to be implemented (e.g. arrays, objects...)
        if (
          fieldValue !== undefined &&
          typeof fieldValue === 'string' &&
          fieldValue.trim().length > 0
        ) {
          httpParams[fieldKey] = <string>fieldValue;
        } else if (fieldValue !== undefined && typeof fieldValue === 'number') {
          httpParams[fieldKey] = <number>fieldValue;
        } else if (fieldValue !== undefined && fieldValue instanceof Array) {
          const paramArray = <Array<any>>fieldValue;
          const paramArrayClean = paramArray.map(item => item.value);
          httpParams[fieldKey] = paramArrayClean;
        }
      });
    }
    // return calculated params
    return httpParams;
  }

  setLoadingDataState(state: boolean) {
    this.loadingSubject.next(state);
  }
}
