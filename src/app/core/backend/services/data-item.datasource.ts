import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map, catchError, finalize } from 'rxjs/operators';
import {
  Observable,
  of as observableOf,
  merge,
  BehaviorSubject,
  Subscription
} from 'rxjs';
import { DataItemService } from './data-item.service';
import { DataResponseContainer } from '../models/data-response-container.type';
import { HttpResponse } from '@angular/common/http';
import { ConstantUtil } from '../../util/constant-util.type';

/**
 * Generic Data source for the view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class DataItemDataSource<
  TemplateItem,
  TemplateService extends DataItemService<TemplateItem>
> extends DataSource<TemplateItem> {
  private dataListSubject = new BehaviorSubject<TemplateItem[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();
  private loadDataSubject$: Subscription;

  private _totalItemCount = 0;
  private _data: TemplateItem[];

  constructor(
    private dataService: TemplateService,
    private paginator?: MatPaginator,
    private sort?: MatSort
  ) {
    super();
  }

  public get totalItemCount() {
    return this._totalItemCount;
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<TemplateItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    return this.dataListSubject.asObservable();
  }

  getDataList(): Observable<TemplateItem[]> {
    return this.dataListSubject.asObservable();
  }

  getData(): TemplateItem[] {
    return this._data;
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.dataListSubject.complete();
    this.loadingSubject.complete();
    this._totalItemCount = 0;
    this._data = null;
  }

  loadData(pageIndex?: number, pageSize?: number, filterDataItem?: any) {
    this.loadingSubject.next(true);
    const sortParam: string = this.sort && this.sort.active && this.sort.direction ? `${this.sort.active},${this.sort.direction}` : null;
    this.loadDataSubject$ = this.dataService
      .findData(pageIndex, pageSize, filterDataItem, sortParam)
      .pipe(
        catchError(() => observableOf([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe((response: HttpResponse<TemplateItem[]>) => {
        if (response !== null && response.headers) {
          const totalCount = +response.headers.get(ConstantUtil.X_TOTAL_COUNT_HEADER);
          this.setDataList(response.body, totalCount);
        }
        if (this.loadDataSubject$) {
          this.loadDataSubject$.unsubscribe();
        }
      });
  }

  setDataList(itemList: TemplateItem[], itemCount: number): any {
    if (itemList) {
      this._totalItemCount = itemCount;
      this._data = itemList;
      this.dataListSubject.next(itemList);
    } else {
      this._totalItemCount = 0;
      this._data = [];
      this.dataListSubject.next([]);
    }
    // loading progress finished
    this.setLoadingDataState(false);
  }

  setLoadingDataState(state: boolean) {
    this.loadingSubject.next(state);
  }
}
