import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';

import { Observable , of as ObservableOf} from 'rxjs';
import { Router } from '@angular/router';
import { catchError, map, tap } from 'rxjs/operators';
import { LoggerService } from '../../util/logger.service';
import { RouteConfig } from '../../../config/route-config';

@Injectable()
export class BackendService {
  constructor(
    private logger: LoggerService,
    private router: Router,
    private http: HttpClient
  ) {}

  getJSON<T>(path: string, defaultErrorResult: any, httpParams?: HttpParams): Observable<T> {
    const logMsg = `method:getJSON; path:${path}; error_result:${defaultErrorResult}`;
    return this.http
      .get<T>('/api/' + path, {params: httpParams, observe: 'response'})
      .pipe(catchError(this.handleError(logMsg, defaultErrorResult)));
  }

  postJSON<T>(path: string, body: any, defaultErrorResult: any): Observable<T> {
    const logMsg = `method:postJSON; path:${path}; error_result:${defaultErrorResult}`;
    return this.http
      .post<T>('/api/' + path, body, {observe: 'response'})
      .pipe(catchError(this.handleError(logMsg, defaultErrorResult)));
  }

  putJSON<T>(path: string, body: any, defaultErrorResult: any): Observable<T> {
    const logMsg = `method:putJSON; path:${path}; error_result:${defaultErrorResult}`;
    return this.http
      .put<T>('/api/' + path, body)
      .pipe(catchError(this.handleError(logMsg, defaultErrorResult)));
  }

  putEmpty<T>(path: string, body: any, defaultErrorResult: any): Observable<T> {
    const logMsg = `method:putEmpty; path:${path}; error_result:${defaultErrorResult}`;
    return this.http
      .put<T>('/api/' + path, body)
      .pipe(catchError(this.handleError(logMsg, defaultErrorResult)));
  }

  delete<T>(path: string, defaultErrorResult: any, httpParams?: HttpParams): Observable<T> {
    const logMsg = `method:delete; path:${path}; error_result:${defaultErrorResult}`;
    const del = this.http
      .delete<T>('/api/' + path)
      .pipe(catchError(this.handleError(logMsg, defaultErrorResult)));

    return del;
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error instanceof HttpErrorResponse) {
          if (error.status === 401) {
            console.log(`Status code: ${error.status}. ${error.statusText}`, `Unauthorized`);
            this.logger.showError(`Status code: ${error.status}. ${error.statusText}`, `Unauthorized`, true);
            this.router.navigate([RouteConfig.DASHBOARD], { queryParams: { logout: null } });
          } else if (error.status === 502) {
            this.logger.showWarning(`${error.error.error}: ${error.error.message}`, `${operation}`, true);
            throw error;
          } else if (error.status === 400) {
            this.logger.showError(`${error.error.error}: ${error.error.message}`, `${operation}`, true);
            throw error;
          } else {
            throw error;
          }
      } else {
        const msg = `${operation}`;
        this.logger.showError(error, msg, true);
      }

      // Let the app keep running by returning an empty result.
      return ObservableOf(result as T);
    };
  }
}
