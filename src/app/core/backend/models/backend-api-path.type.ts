export const BackendApiPath = {
    ACCOUNT_RESOURCE : {
        GET_ACCOUNT: 'account',
        AUTHENTICATE: 'authenticate'
    },
    BOOK_RESOURCE : {
        GET_ALL: 'books',
        CREATE_UPDATE_ONE: 'books/',
        GET_DELETE_ONE: 'books/{id}',
        GET_BOOK_GENRES_ALL: 'books/book-genres'
    },
    USER_MANAGEMENT_RESOURCE : {
        GET_ALL: 'users',
        GET_AUTHORITIES: 'users/authorities',
        GET_ONE: 'users/{userName}',
        CREATE_UPDATE_ONE: 'users/',
    }
};
