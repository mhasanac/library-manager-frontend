export class DataResponseContainer<T> {
  constructor(public dataList: T[], public totalCount: number) {}
}
