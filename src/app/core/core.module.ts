import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';

import { AuthModule } from './authentication/auth.module';
import { LoggerService } from 'src/app/core/util/logger.service';
import { BackendService } from 'src/app/core/backend/services/backend.service';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';


// move providers to variable so we can append to it dynamically
const providers: any[] = [
  LoggerService,
  BackendService
];

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule, // required animations module
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthModule.forRoot(),
    MaterialModule,
    FlexLayoutModule
  ],
  declarations: [

   ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  providers: providers
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
        throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: providers
    };
  }
}
