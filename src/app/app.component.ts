import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { AuthenticationService } from './core/authentication/services/authentication.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Library manager frontend';
  constructor(public authenticationService: AuthenticationService) {
  }
}
