import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { DashboardModule } from './dashboard/dashboard.module';
import { BookManagementModule } from './book-management/book-management.module';
import { AppCommonModule } from './common/app-common.module';
import { LoginModule } from './login/login.module';

@NgModule({
  imports: [
    DashboardModule.forRoot(),
    BookManagementModule.forRoot(),
    AppCommonModule.forRoot(),
    LoginModule.forRoot()
  ],
  declarations: [],
  entryComponents: [],
  exports: [],
  providers: []
})
export class ViewsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ViewsModule,
      providers: []
    };
  }
}
