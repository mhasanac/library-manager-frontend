import {
  Component,
  OnInit,
  Inject,
  AfterViewInit,
  OnDestroy,
  ViewChild
} from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatPaginator,
  MatSort
} from '@angular/material';

import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  tap,
  startWith,
  map,
  debounceTime,
  switchMap,
  finalize,
  takeUntil,
  take
} from 'rxjs/operators';
import { LoggerService } from 'src/app/core/util/logger.service';
import { BookItem } from '../models/book-item.type';
import { BookGenre } from '../models/book-genre-item.type';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-book-detail-dialog',
  templateUrl: './book-detail-dialog.component.html',
  styleUrls: ['./book-detail-dialog.component.css']
})
export class BookDetailDialogComponent implements OnInit, OnDestroy {

  private formErrors: any;
  private bookItem: BookItem;

  public isEdit: boolean;
  public form: FormGroup;
  public bookGenreList: BookGenre[];

  constructor(
    public dialogRef: MatDialogRef<BookDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private loggerService: LoggerService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe
  ) {
    this.bookGenreList = data.bookGenreList;
    this.bookItem = data.item;

    this.isEdit = this.bookItem ? true : false;
    // Reactive form errors
    this.formErrors = {
      name:  [null],
      description:  [null],
      pageCount:  [null],
      author:  [null],
      publisher:  [null],
      bookGenre:  [null],
      createdDate:  [null],
      updatedDate:  [null]
    };
  }

  ngOnInit() {
    // Reactive Form
    this.form = this.formBuilder.group({
      name: [{value: this.bookItem ? this.bookItem.name : null, disabled: false }, Validators.required],
      description: [{value: this.bookItem ? this.bookItem.description : null, disabled: false }],
      pageCount: [{value: this.bookItem ? this.bookItem.pageCount : null, disabled: false }, Validators.required],
      author: [{value: this.bookItem ? this.bookItem.author : null, disabled: false }, Validators.required],
      publisher: [{value: this.bookItem ? this.bookItem.publisher : null, disabled: false }, Validators.required],
      bookGenre: [{value: this.bookItem ? this.bookItem.bookGenre.id : null, disabled: false }, Validators.required],
      createdDate: [{value: this.bookItem ?
            this.datePipe.transform(this.bookItem.createdDate, 'dd.MM.yyyy HH:mm:ss') : null, disabled: true }],
      updatedDate: [{value: this.bookItem ?
            this.datePipe.transform(this.bookItem.updatedDate, 'dd.MM.yyyy HH:mm:ss') : null, disabled: true }],
    });

    this.form.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });
  }

  onFormValuesChanged() {
    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.formErrors[field] = {};

      // Get the control
      const control = this.form.get(field);

      if (control && control.dirty && !control.valid) {
        this.formErrors[field] = control.errors;
      }
    }
  }

  ngOnDestroy() {
    // destroy datasources
  }

  onResetForm() {
    this.form.reset();
  }

  confirmDialog() {
    this.loggerService.debug('BookDetailDialogComponent', 'saved');
    const bookItem: BookItem = {
      id: this.bookItem ? this.bookItem.id : null,
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      pageCount: this.form.controls.pageCount.value,
      author: this.form.controls.author.value,
      publisher: this.form.controls.publisher.value,
      bookGenre: new BookGenre(this.form.controls.bookGenre.value, null),
      createdDate: this.bookItem ? this.bookItem.createdDate : null,
      updatedDate: this.bookItem ? this.bookItem.updatedDate : null
    };
    this.dialogRef.close(bookItem);
  }

  closeDialog() {
    this.loggerService.debug('BookDetailDialogComponent', 'closed');
    this.dialogRef.close(false);
  }
}
