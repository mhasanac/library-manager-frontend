import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs/Observable';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { AuthService } from 'src/app/services/auth.service';
import { DataItemDataSource } from 'src/app/core/backend/services/data-item.datasource';
import { BookService } from '../services/book.service';
import { BookGenre } from '../models/book-genre-item.type';
import { BookGenreService } from '../services/book-genre.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from 'src/app/core/util/logger.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication/services/authentication.service';
import { merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BookItem } from '../models/book-item.type';
import { ConfirmationDialogComponent } from 'src/app/views/common/confirmation-dialog/confirmation-dialog.component';
import { BookDetailDialogComponent } from '../book-detail-dialog/book-detail-dialog.component';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-book-overview',
  templateUrl: './book-overview.component.html',
  styleUrls: ['./book-overview.component.css']
})
export class BookOverviewComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  displayedColumns = [
    'name',
    'author',
    'publisher',
    'bookGenre',
    'createdDate',
    'updatedDate',
    'actions'
  ];

  public bookDataSource: DataItemDataSource<
    BookItem,
    BookService
  >;
  public bookGenreDataSource: DataItemDataSource<
    BookGenre,
    BookGenreService
  >;

  public form: FormGroup;
  private formErrors: any;
  public formState: any;

  constructor(
    private bookService: BookService,
    private bookGenreService: BookGenreService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private loggerService: LoggerService,
    private router: Router,
    public authenticationService: AuthenticationService
  ) {
    // Reactive form errors
    this.formErrors = {
      name: {},
      author: {},
      publisher: {},
      bookGenre: {}
    };

    this.formState = {
      name: false,
      author: false,
      publisher: false,
      bookGenre: false
    };
  }

  ngOnInit() {
    this.bookDataSource = new DataItemDataSource<
    BookItem,
    BookService
    >(this.bookService, this.paginator, this.sort);

    this.bookGenreDataSource = new DataItemDataSource<
    BookGenre,
    BookGenreService
    >(this.bookGenreService, null, null);

    // load initial data
    this.bookDataSource.loadData();
    this.bookGenreDataSource.loadData();
    // Reactive Form
    this.form = this.formBuilder.group({
      name: [null, null],
      author: [null, null],
      publisher: [null, null],
      bookGenre: [null, null]
    });

    this.form.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });
  }

  onFormValuesChanged() {
    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.formErrors[field] = {};

      // Get the control
      const control = this.form.get(field);

      if (control && control.dirty && !control.valid) {
        this.formErrors[field] = control.errors;
      }
    }
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(tap(() => this.loadPage()))
      .subscribe();
  }

  ngOnDestroy() {
    // destroy datasources
    if (this.bookDataSource) {
      this.bookDataSource.disconnect();
    }
    if (this.bookGenreDataSource) {
      this.bookGenreDataSource.disconnect();
    }
  }

  onSearchForm() {
    this.paginator.pageIndex = 0;
    this.loadPage();
  }

  onResetForm() {
    this.form.reset();
  }

  loadPage() {
    this.bookDataSource.loadData(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.form.value
    );
  }

  editItem(item: BookItem) {
    this.openDetailDialog(item);
  }

  addNewItem() {
    this.openDetailDialog();
  }

  openDetailDialog(bookItem?: BookItem) {
    const bookDetailDialogRef = this.dialog.open(
      BookDetailDialogComponent,
      {
        width: '700px',
        panelClass: 'book-detail-dialog-panel',
        data: {
          item: bookItem,
          bookGenreList: this.bookGenreDataSource.getData()
        }
      }
    );

    bookDetailDialogRef
      .afterClosed()
      .subscribe((item: BookItem) => {
        if (item) {
          this.bookDataSource.setLoadingDataState(true);
          this.bookService
            .saveItem(item)
            .subscribe((content: BookItem) => {
              // debugger;
             /* if (orderItem) {
                this.loggerService.infoWithToast(content);
                this.onSearchForm();
              }*/
              this.onSearchForm();
            });
        }
      });
  }

  deleteItem(item: BookItem) {
    if (!this.authenticationService.isAuthenticated()) {
      return;
    }

    const confirmationDialogRef = this.dialog.open(
      ConfirmationDialogComponent,
      {
        width: '400px',
        panelClass: 'confirmation-dialog-panel',
        data: {
          name: item.name,
          item: item
        }
      }
    );

    confirmationDialogRef
      .afterClosed()
      .subscribe((bookItem: BookItem) => {
        if (bookItem) {
          this.bookDataSource.setLoadingDataState(true);
          this.bookService
            .deleteItem(bookItem)
            .subscribe((content: BookItem) => {
             /* if (orderItem) {
                this.loggerService.infoWithToast(content);
                this.onSearchForm();
              }*/
              this.onSearchForm();
            });
        }
      });
  }
}
