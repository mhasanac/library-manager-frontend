import { BookGenre } from './book-genre-item.type';

export class BookItem {
    id: number;
    name: string;
    description: string;
    pageCount: number;
    author: string;
    publisher: string;
    bookGenre: BookGenre;
    createdDate: Date;
    updatedDate: Date;
}
