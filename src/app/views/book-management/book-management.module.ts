import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CoreModule } from 'src/app/core/core.module';
import { BookOverviewComponent } from './book-overview/book-overview.component';
import { BookDetailDialogComponent } from './book-detail-dialog/book-detail-dialog.component';
import { BookService } from './services/book.service';
import { BookGenreService } from './services/book-genre.service';
import { AppCommonModule } from '../common/app-common.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule.forRoot(),
    AppCommonModule.forRoot()
  ],
  declarations: [
    BookOverviewComponent,
    BookDetailDialogComponent
  ],
  entryComponents: [BookDetailDialogComponent],
  exports: [
    BookOverviewComponent
  ],
  providers: [BookService, BookGenreService, DatePipe]
})
export class BookManagementModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BookManagementModule,
      providers: []
    };
  }
}
