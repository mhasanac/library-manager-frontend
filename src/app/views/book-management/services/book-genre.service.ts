import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable, of as ObservableOf } from 'rxjs';
import { LoggerService } from '../../../core/util/logger.service';
import { BackendService } from '../../../core/backend/services/backend.service';
import { DataItemService } from '../../../core/backend/services/data-item.service';
import { DataResponseContainer } from '../../../core/backend/models/data-response-container.type';
import { BackendApiPath } from '../../../core/backend/models/backend-api-path.type';
import { BookGenre } from '../models/book-genre-item.type';

@Injectable()
export class BookGenreService extends DataItemService<BookGenre> {
  constructor(
    private logger: LoggerService,
    private backendService: BackendService,
  ) {
    super();
  }

  findData(pageIndex: number, pageSize: number, filterData: any): Observable<HttpResponse<BookGenre[]>> {
    // populate http params
    const httpParams: any = this.getPopulatedHttpParams(pageIndex, pageSize, filterData);

    return this.backendService
          .getJSON<HttpResponse<BookGenre[]>>(BackendApiPath.BOOK_RESOURCE.GET_BOOK_GENRES_ALL, null, httpParams);
  }
}
