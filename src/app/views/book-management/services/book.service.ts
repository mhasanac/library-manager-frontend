import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable, of as ObservableOf } from 'rxjs';
import { LoggerService } from '../../../core/util/logger.service';
import { BackendService } from '../../../core/backend/services/backend.service';
import { DataItemService } from '../../../core/backend/services/data-item.service';
import { DataResponseContainer } from '../../../core/backend/models/data-response-container.type';
import { BackendApiPath } from '../../../core/backend/models/backend-api-path.type';
import { BookItem } from '../models/book-item.type';

@Injectable()
export class BookService extends DataItemService<BookItem> {
  constructor(
    private logger: LoggerService,
    private backendService: BackendService,
  ) {
    super();
  }

  findData(pageIndex: number, pageSize: number, filterData: any, sortParam: string): Observable<HttpResponse<BookItem[]>> {
    // populate http params
    const httpParams: any = this.getPopulatedHttpParams(pageIndex, pageSize, filterData, sortParam);

    return this.backendService.getJSON<HttpResponse<BookItem[]>>(BackendApiPath.BOOK_RESOURCE.GET_ALL, null, httpParams);
  }

  deleteItem(item: BookItem): Observable<BookItem> {
    this.setLoadingDataState(true);
    const deletePath = BackendApiPath.BOOK_RESOURCE.GET_DELETE_ONE.replace('{id}', item.id.toString());
    return this.backendService.delete<BookItem>(deletePath, null);
  }

  saveItem(item: BookItem): Observable<BookItem> {
    this.setLoadingDataState(true);
    const savePath = BackendApiPath.BOOK_RESOURCE.CREATE_UPDATE_ONE;
    if (item.id) {
      return this.backendService.putJSON<BookItem>(savePath, item, null);
    }
    return this.backendService.postJSON<BookItem>(savePath, item, null);
  }
}
