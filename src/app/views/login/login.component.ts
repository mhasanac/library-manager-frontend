import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { LoggerService } from '../../core/util/logger.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/core/authentication/services/authentication.service';
import { UserProfile } from 'src/app/core/authentication/models/user.type';
import { RouteConfig } from '../../config/route-config';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  private formErrors: any;
  public formState: any;
  public loading = false;

  constructor(
    private formBuilder: FormBuilder,
    public loggerService: LoggerService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {

    this.formErrors = {
        name: {},
        password: {}
      };

      this.formState = {
        email: false,
        password: false
      };
  }

     /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            email   : ['', [
                Validators.required,
                Validators.email
                ]
            ],
            password: ['', Validators.required]
        });


        this.loginForm.valueChanges.subscribe(() => {
            this.onFormValuesChanged();
          });
    }

    onFormValuesChanged() {
        for (const field in this.formErrors) {
          if (!this.formErrors.hasOwnProperty(field)) {
            continue;
          }

          // Clear previous errors
          this.formErrors[field] = {};

          // Get the control
          const control = this.loginForm.get(field);

          if (control && control.dirty && !control.valid) {
            this.formErrors[field] = control.errors;
          }
        }
      }

    login(): void {
        this.loading = true;
        this.authenticationService
          .login(this.loginForm.controls.email.value, this.loginForm.controls.password.value)
            .subscribe((userProfile: UserProfile) => {
                if (userProfile) {
                    this.router.navigate([RouteConfig.DASHBOARD], {
                        queryParams: { logout: null }
                    });
                } else {
                    this.loggerService.showError('Login failed!', 'Please check your credentials!', true);
                }
                this.loading = false;
        });
    }
}
