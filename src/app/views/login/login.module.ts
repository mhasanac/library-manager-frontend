import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule.forRoot()
  ],
  declarations: [LoginComponent],
  entryComponents: [],
  exports: [LoginComponent],
  providers: []
})
export class LoginModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LoginModule,
      providers: []
    };
  }
}
