import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { LoginComponent } from './login.component';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../core/material/material.module';

xdescribe('LoginComponent', () => {
    /*let comp: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let de: DebugElement;
    let element: HTMLElement;

    let loggerService: LoggerService;
    let authService: AuthService;
    let router: Router;

    let loggerServiceSpy: jasmine.Spy;
    let routerSpy: jasmine.Spy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [CommonModule, MaterialModule],
            declarations: [LoginComponent],
            providers: [
                {provide: AuthService, useClass: AuthServiceStub},
                {provide: Router, useClass: RouterStub},
                {provide: LoggerService, useClass: LoggerServiceStub},
                {provide: APP_BASE_HREF, useValue: '/'}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement.query(By.css('.login-container'));
        element = de.nativeElement;

        authService = fixture.debugElement.injector.get(AuthService);
        loggerService = fixture.debugElement.injector.get(LoggerService);
        router = fixture.debugElement.injector.get(Router);
    });

    it('for empty input fields login button is disabled', async(() => {
        fixture.detectChanges();

        let loginButton = fixture.debugElement.query(By.css('#loginButton'));
        
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            expect(loginButton.nativeElement.disabled).toBeTruthy();
        });
    }));

    it('for filled input fields login button is enabled', async(() => {
        comp.username = 'test';
        comp.password = 'test';

        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let loginButton = fixture.debugElement.query(By.css('#loginButton'));
            expect(loginButton.nativeElement.disabled).toBeFalsy();
        })
    }));

    it('on login button click with correct credentials navigate to overview and display success', async(() => {
        loggerServiceSpy = spyOn(loggerService, 'infoWithToast').and.callThrough();
        routerSpy = spyOn(router, 'navigate');

        comp.username = 'test';
        comp.password = 'test';
        fixture.detectChanges(); 
       
        fixture.whenStable().then(() => {
            fixture.detectChanges();

            let loginButton = fixture.debugElement.query(By.css('#loginButton'));

            click(loginButton);

            const navArgs = routerSpy.calls.first().args[0];
            expect(navArgs[0]).toBe('/my-items');

            expect(loggerServiceSpy).toHaveBeenCalled();
        });
    }));

    it('on login button click with incorrect credentials display error', async(() => {
        loggerServiceSpy = spyOn(loggerService, 'errorWithToast');
        
        comp.username = 'wrong';
        comp.password = 'wrong';
        fixture.detectChanges();

        fixture.whenStable().then(() => {
            fixture.detectChanges();

            let loginButton = fixture.debugElement.query(By.css('#loginButton'));

            click(loginButton);

            expect(loggerServiceSpy).toHaveBeenCalled();

            const loggerArgs = loggerServiceSpy.calls.first().args[0];
            expect(loggerArgs).toBe('Login fehlgeschlagen');
        });
        
    }));*/
});
