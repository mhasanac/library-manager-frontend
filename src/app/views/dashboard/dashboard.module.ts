import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
    imports: [
      CommonModule,
      CoreModule.forRoot()
    ],
    declarations: [
        DashboardComponent
    ],
    entryComponents: [
    ],
    exports: [
        DashboardComponent
    ],
    providers: [
    ]
  })
  export class DashboardModule {
    static forRoot(): ModuleWithProviders {
      return {
        ngModule: DashboardModule,
        providers: []
      };
    }
  }

