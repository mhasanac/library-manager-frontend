import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
    imports: [
      CommonModule,
      CoreModule.forRoot()
    ],
    declarations: [
      ConfirmationDialogComponent,
    ],
    entryComponents: [
      ConfirmationDialogComponent,
    ],
    providers: [
    ]
  })
  export class AppCommonModule {
    static forRoot(): ModuleWithProviders {
      return {
        ngModule: AppCommonModule,
        providers: []
      };
    }
  }

