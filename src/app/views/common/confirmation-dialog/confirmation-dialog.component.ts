import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoggerService } from '../../../core/util/logger.service';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {
  public model = {
    name: null,
    item: null
  };

  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private logger: LoggerService
  ) {
    this.model.name = data.name;
    this.model.item = data.item;
  }

  ngOnInit() {
    console.log(this.data);
  }

  confirmDialog() {
    this.logger.debug('ConfirmationDialogComponent', 'saved');
    this.dialogRef.close(this.model.item);
  }

  closeDialog() {
    this.logger.debug('ConfirmationDialogComponent', 'closed');
    this.dialogRef.close(null);
  }
}
