export const RouteConfig = {
  LOGIN: '/login',
  DASHBOARD: '/dashboard',
  BOOK: {
    OVERVIEW: 'book-management/overview',
  },
  USER_MANAGEMENT: {
    OVERVIEW: '/user-management/overview',
  }
};
