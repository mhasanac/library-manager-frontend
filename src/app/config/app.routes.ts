import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../views/dashboard/dashboard.component';
import { AuthAdminGuard } from 'src/app/core/authentication/guards/auth-admin.guard';
import { AuthGuard } from '../core/authentication/guards/auth.guard';
import { BookOverviewComponent } from 'src/app/views/book-management/book-overview/book-overview.component';
import { LoginComponent } from '../views/login/login.component';

const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  /*{
    path: 'user-management',
    canActivate: [AuthAdminGuard],
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      {
        path: 'overview',
        component: UserManagementOverviewComponent,
      }
    ]
  },*/

  {
    path: 'book-management',
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      {
        path: 'overview',
        component: BookOverviewComponent
      }
    ]
  },

  // otherwise redirect to home
  {
    path: '**',
    redirectTo: '',
    canActivate: [AuthGuard]
  }
];

export const AppRouting = RouterModule.forRoot(appRoutes);
