import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FormsModule } from '@angular/forms';

import { MaterialModule } from './core/material/material.module';

import { AppComponent } from './app.component';
import { ViewsModule } from './views/views.module';

import { NgSelectModule } from '@ng-select/ng-select';
import { CoreModule } from 'src/app/core/core.module';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRouting } from './config/app.routes';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    AppRouting,
    // App modules
    CoreModule.forRoot(),
    ViewsModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
