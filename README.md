# Library manager frontend - Frontend app

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.

## Setup

* `npm install -g @angular/cli`: Install Angular Command Line Interface globally
* `yarn`: Install application dependencies
* `yarn start`: Run application in development mode at <http://localhost:4200>

## Development server

* Proxy has been setup to redirect all API calls to localhost:8080. So in order to connect to backend run spring boot app on that port or change the port in proxy.conf.json file
* Users:
* **ADMIN user (has all posibilities):**
    * Username: admin@localhost
    * Password: admin
* **PLAIN USER (can't delete book):**
    * Username: user@localhost
    * Password: user

## IDE

The application was developped using **Visual Studio Code**. It's recommended to use at least the following _extensions_:

* **TSLint** for TypeScript linting
* **vscode-icons** to have more specific Icons in the VSCode explorer, including Angular specific icons
* **EditorConfig for VS Code** Support for EditorConfig (.editorconfig) files
* **Debugger for Chrome** Allows to start debugging in VSCode connecting to Google Chrome browser
* **Angular Language Service** Intellisense for Angular Templates

## Style Guide

We follow the official Angular2 style guide:

* Folder structure: <https://angular.io/docs/ts/latest/guide/style-guide.html#!#file-tree>
* Linting: <https://palantir.github.io/tslint/>
  * Visual Code plugin needs to be installed manually
* Additional style guide:
  * Postfix observable variables with a $: myObservable$ (see <http://stackoverflow.com/questions/37671700/angular2-style-guide-property-with-dollar-sign>)
  * Use myObservable$sub for observables Subscription objects
  * Logging: Use core/LoggerService

## Manual Configurations

### Frameworks

* angular material: <https://material.angular.io/>

## Testing

* `ng test` run unit tests

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `yarn build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `yarn test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

####  E2E Testing
For executing E2E tests please use : 
* `npm run e2e`